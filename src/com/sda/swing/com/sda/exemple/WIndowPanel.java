package com.sda.swing.com.sda.exemple;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class WIndowPanel {
    private JButton button;
    private JPanel mainPanel;
    private JLabel HalloWorld;
    private double counter;

    public JButton getButton() {
        return button;
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public WIndowPanel() {
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("jeden");
                super.mouseClicked(e);
                HalloWorld.setText(" Hallo World !" + counter);
                counter++;
            }
        });

        button.addMouseListener(new MouseAdapter() {
        });
    }
}
