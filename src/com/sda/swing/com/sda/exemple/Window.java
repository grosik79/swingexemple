package com.sda.swing.com.sda.exemple;

import javax.swing.*;
import java.awt.*;

public class Window {
    private JFrame frame;
    private WIndowPanel panel;

    public Window(){
    this.panel = new WIndowPanel();
        this.frame = new JFrame();
        this.frame.setContentPane(this.panel.getMainPanel());

        this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        //this.frame.setResizable(false);
        this.frame.setPreferredSize(new Dimension(640,480));
        this.frame.pack();

    }


    public void setVisible(boolean b) {
        frame.setVisible(b);
    }
}
