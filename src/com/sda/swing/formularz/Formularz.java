package com.sda.swing.formularz;

import com.sda.swing.com.sda.exemple.WIndowPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Formularz {
    private JTextField textField1;
    private JTextField textField2;
    private JButton zapiszButton;
    private JTextField textField3;
    private JRadioButton kobietaRadioButton;
    private JRadioButton mężczyznaRadioButton;
    private JComboBox comboBox1;
    private JSpinner spinner1;
    private JButton czyśćButton;
    private JFrame frame;
    private WIndowPanel panel;

    public Formularz() {

        frame = new JFrame();
        this.frame.setContentPane(this.panel.getMainPanel());
        this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.frame.setPreferredSize(new Dimension(640,480));
        this.frame.pack();

        zapiszButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);


            }
        });
        czyśćButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
            }
        });
    }

    public void setVisible(boolean visible) {
        this.frame.setVisible(visible);
    }
}