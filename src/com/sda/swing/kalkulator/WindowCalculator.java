package com.sda.swing.kalkulator;

import javax.swing.*;
import java.awt.*;

public class WindowCalculator {
    private JFrame frame;
    private Calculator panel;

    public WindowCalculator(){
        this.panel = new Calculator();
        this.frame = new JFrame();
        this.frame.setContentPane(this.panel.getMainPanel());

        this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.frame.setPreferredSize(new Dimension(300,450));
        this.frame.pack();
    }
    public void setVisibile(boolean b){
        frame.setVisible(b);
    }
}
